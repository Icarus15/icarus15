## Introduction

IcarusCore is a *MMORPG* Framework based mostly in C++.

It is derived from *TrinityCore*, the *Massive Network Game Object Server*, and is
based on the code of that project with extensive changes over time to optimize,
improve and cleanup the codebase at the same time as improving the in-game
mechanics and functionality.

It is completely private source; 

## Requirements

+ Platform: Linux, Windows or Mac
+ Processor with SSE2 support
+ ACE ≥ 5.8.3 (included for Windows)
+ MySQL ≥ 5.1.0 (included for Windows)
+ CMake ≥ 2.8.11.2 / 2.8.9 (Windows / Linux)
+ OpenSSL ≥ 1.0.0
+ GCC ≥ 4.7.2 (Linux only)
+ MS Visual Studio ≥ 12 (2013) (Windows only)


## Copyright Icarus Gaming 2014 - 2018

## Links

[Site](http://icarus-gaming.org)

[Forums](http://icarus-gaming.org/forum)
