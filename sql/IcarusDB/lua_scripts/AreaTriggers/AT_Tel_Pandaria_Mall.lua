local NPC_ID = 129991 -- Invisible Word Trigger (Kara Test Tel)
local Teleport_Range = 1 -- Range in yards
     
function AreaTrigger_OnSpawn(event, creature)
        creature:SetVisible(false)
        creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Teleport_Range)) then
                plr:Teleport(870, 3818.6, 1793.1, 950.3, 5) 
        end
end
  
RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)
