local NPC_ID = 129993 -- Brawnl'gar Referee npc id
local Fight_Range = 16 -- Adjustable range check
     
function AreaTrigger_OnSpawn(event, creature)
    creature:SetVisible(false)
    creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Fight_Range)) then
            plr:SetFFA(true)
				print("Players in range and set ffa")
	end
end

RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)