-- #############################
-- ##    Shared by RazorX14   ##
-- ##       For BotCore       ##
-- #############################


--> PUBLIC HOUSE TELEPORTER CODE


local TELEPORTER_ID = 80009


--> DO NOT ALTER ANYTHING BELOW THIS POINT!!!

local function GetNameFromPublicHouses1(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 1")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses2(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 2")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses3(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 3")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses4(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 4")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses5(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 5")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses6(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 6")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses7(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 7")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses8(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 8")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses9(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 9")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses10(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 10")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses11(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 11")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses12(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 12")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses13(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 13")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses14(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 14")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses15(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 15")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses16(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 16")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses17(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 17")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses18(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 18")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses19(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 19")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetNameFromPublicHouses20(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 20")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end

local function GetCoords1(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords2(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords3(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords4(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords5(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords6(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords7(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords8(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords9(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords10(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords11(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords12(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords13(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords14(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords15(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords16(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords17(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords18(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords19(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords20(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords21(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords22(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords23(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords24(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords25(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords26(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords27(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords28(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords29(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords30(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords31(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords32(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords33(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords34(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords35(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords36(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords37(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords38(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords39(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords40(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords41(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords42(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords43(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords44(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords45(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords46(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords47(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords48(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords49(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords50(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords51(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords52(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords53(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords54(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords55(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords56(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords57(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords58(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords59(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords60(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords61(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords62(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords63(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords64(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords65(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords66(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords67(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords68(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords69(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords70(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords71(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords72(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords73(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords74(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords75(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords76(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords77(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords78(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords79(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords80(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords81(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords82(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords83(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords84(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords85(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords86(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords87(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords88(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords89(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords90(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords91(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords92(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords93(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords94(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords95(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords96(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `public_houses_locs` WHERE `PublicHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords97(playername)
	local Coords = WorldDBQuery("SELECT X FROM `public_houses_locs` WHERE `PublicHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords98(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `public_houses_locs` WHERE `PublicHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords99(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `public_houses_locs` WHERE `PublicHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords100(playername)
	local Coords = WorldDBQuery("SELECT O FROM `public_houses_locs` WHERE `PublicHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end

function DelayMessage1(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses1(player:GetName()).."'s Public House")
end
function DelayMessage2(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses2(player:GetName()).."'s Public House")
end
function DelayMessage3(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses3(player:GetName()).."'s Public House")
end
function DelayMessage4(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses4(player:GetName()).."'s Public House")
end
function DelayMessage5(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses5(player:GetName()).."'s Public House")
end
function DelayMessage6(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses6(player:GetName()).."'s Public House")
end
function DelayMessage7(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses7(player:GetName()).."'s Public House")
end
function DelayMessage8(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses8(player:GetName()).."'s Public House")
end
function DelayMessage9(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses9(player:GetName()).."'s Public House")
end
function DelayMessage10(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses10(player:GetName()).."'s Public House")
end
function DelayMessage11(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses11(player:GetName()).."'s Public House")
end
function DelayMessage12(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses12(player:GetName()).."'s Public House")
end
function DelayMessage13(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses13(player:GetName()).."'s Public House")
end
function DelayMessage14(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses14(player:GetName()).."'s Public House")
end
function DelayMessage15(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses15(player:GetName()).."'s Public House")
end
function DelayMessage16(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses16(player:GetName()).."'s Public House")
end
function DelayMessage17(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses17(player:GetName()).."'s Public House")
end
function DelayMessage18(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses18(player:GetName()).."'s Public House")
end
function DelayMessage19(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses19(player:GetName()).."'s Public House")
end
function DelayMessage20(player)
	player:SendNotification("Welcome To "..GetNameFromPublicHouses20(player:GetName()).."'s Public House")
end

local function GossipMenu(event, player, unit)
    local PlrName = player:GetName()
		player:GossipMenuAddItem(2, "[01] "..GetNameFromPublicHouses1(player:GetName()).."'s Public House", 0, 2, 0)
		player:GossipMenuAddItem(2, "[02] "..GetNameFromPublicHouses2(player:GetName()).."'s Public House", 0, 3, 0)
		player:GossipMenuAddItem(2, "[03] "..GetNameFromPublicHouses3(player:GetName()).."'s Public House", 0, 4, 0)
		player:GossipMenuAddItem(2, "[04] "..GetNameFromPublicHouses4(player:GetName()).."'s Public House", 0, 5, 0)
		player:GossipMenuAddItem(2, "[05] "..GetNameFromPublicHouses5(player:GetName()).."'s Public House", 0, 6, 0)
		player:GossipMenuAddItem(2, "[06] "..GetNameFromPublicHouses6(player:GetName()).."'s Public House", 0, 7, 0)
		player:GossipMenuAddItem(2, "[07] "..GetNameFromPublicHouses7(player:GetName()).."'s Public House", 0, 8, 0)
		player:GossipMenuAddItem(2, "[08] "..GetNameFromPublicHouses8(player:GetName()).."'s Public House", 0, 9, 0)
		player:GossipMenuAddItem(2, "[09] "..GetNameFromPublicHouses9(player:GetName()).."'s Public House", 0, 10, 0)
		player:GossipMenuAddItem(2, "[10] "..GetNameFromPublicHouses10(player:GetName()).."'s Public House", 0, 11, 0)
		player:GossipMenuAddItem(2, "[11] "..GetNameFromPublicHouses11(player:GetName()).."'s Public House", 0, 12, 0)
		player:GossipMenuAddItem(2, "[12] "..GetNameFromPublicHouses12(player:GetName()).."'s Public House", 0, 13, 0)
		player:GossipMenuAddItem(2, "[13] "..GetNameFromPublicHouses13(player:GetName()).."'s Public House", 0, 14, 0)
		player:GossipMenuAddItem(2, "[14] "..GetNameFromPublicHouses14(player:GetName()).."'s Public House", 0, 15, 0)
		player:GossipMenuAddItem(2, "[15] "..GetNameFromPublicHouses15(player:GetName()).."'s Public House", 0, 16, 0)
		player:GossipMenuAddItem(2, "[16] "..GetNameFromPublicHouses16(player:GetName()).."'s Public House", 0, 17, 0)
		--player:GossipMenuAddItem(2, "[17] "..GetNameFromPublicHouses17(player:GetName()).."'s Public House", 0, 18, 0)
		--player:GossipMenuAddItem(2, "[18] "..GetNameFromPublicHouses18(player:GetName()).."'s Public House", 0, 19, 0)
		--player:GossipMenuAddItem(2, "[19] "..GetNameFromPublicHouses19(player:GetName()).."'s Public House", 0, 20, 0)
		--player:GossipMenuAddItem(2, "[20] "..GetNameFromPublicHouses20(player:GetName()).."'s Public House", 0, 21, 0)
		player:GossipSendMenu(1, unit)
end

local function GossipSelect(event, player, unit, sender, intid, code)
	if (intid == 2) then
		if GetNameFromPublicHouses1(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords1(player:GetName()),GetCoords2(player:GetName()),GetCoords3(player:GetName()),GetCoords4(player:GetName())+2,GetCoords5(player:GetName()))
			CreateLuaEvent(function() DelayMessage1(player); end, 3000, 1)
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 3) then
		if GetNameFromPublicHouses2(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords6(player:GetName()),GetCoords7(player:GetName()),GetCoords8(player:GetName()),GetCoords9(player:GetName())+2,GetCoords10(player:GetName()))
			CreateLuaEvent(function() DelayMessage2(player); end, 3000, 1)
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 4) then
		if GetNameFromPublicHouses3(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords11(player:GetName()),GetCoords12(player:GetName()),GetCoords13(player:GetName()),GetCoords14(player:GetName())+2,GetCoords15(player:GetName()))
			CreateLuaEvent(function() DelayMessage3(player); end, 3000, 1)
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 5) then
		if GetNameFromPublicHouses4(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords16(player:GetName()),GetCoords17(player:GetName()),GetCoords18(player:GetName()),GetCoords19(player:GetName())+2,GetCoords20(player:GetName()))
			CreateLuaEvent(function() DelayMessage4(player); end, 3000, 1)
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 6) then
		if GetNameFromPublicHouses5(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords21(player:GetName()),GetCoords22(player:GetName()),GetCoords23(player:GetName()),GetCoords24(player:GetName())+2,GetCoords25(player:GetName()))
			CreateLuaEvent(function() DelayMessage5(player); end, 3000, 1)
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 7) then
		if GetNameFromPublicHouses6(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords26(player:GetName()),GetCoords27(player:GetName()),GetCoords28(player:GetName()),GetCoords29(player:GetName())+2,GetCoords30(player:GetName()))
			CreateLuaEvent(function() DelayMessage6(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 8) then
		if GetNameFromPublicHouses7(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords31(player:GetName()),GetCoords32(player:GetName()),GetCoords33(player:GetName()),GetCoords34(player:GetName())+2,GetCoords35(player:GetName()))
			CreateLuaEvent(function() DelayMessage7(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 9) then
		if GetNameFromPublicHouses8(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords36(player:GetName()),GetCoords37(player:GetName()),GetCoords38(player:GetName()),GetCoords39(player:GetName())+2,GetCoords40(player:GetName()))
			CreateLuaEvent(function() DelayMessage8(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 10) then
		if GetNameFromPublicHouses9(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords41(player:GetName()),GetCoords42(player:GetName()),GetCoords43(player:GetName()),GetCoords44(player:GetName())+2,GetCoords45(player:GetName()))
			CreateLuaEvent(function() DelayMessage9(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 11) then
		if GetNameFromPublicHouses10(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords46(player:GetName()),GetCoords47(player:GetName()),GetCoords48(player:GetName()),GetCoords49(player:GetName())+2,GetCoords50(player:GetName()))
			CreateLuaEvent(function() DelayMessage10(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 12) then
		if GetNameFromPublicHouses11(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords51(player:GetName()),GetCoords52(player:GetName()),GetCoords53(player:GetName()),GetCoords54(player:GetName())+2,GetCoords55(player:GetName()))
			CreateLuaEvent(function() DelayMessage11(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 13) then
		if GetNameFromPublicHouses12(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords56(player:GetName()),GetCoords57(player:GetName()),GetCoords58(player:GetName()),GetCoords59(player:GetName())+2,GetCoords60(player:GetName()))
			CreateLuaEvent(function() DelayMessage12(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 14) then
		if GetNameFromPublicHouses13(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords61(player:GetName()),GetCoords62(player:GetName()),GetCoords63(player:GetName()),GetCoords64(player:GetName())+2,GetCoords65(player:GetName()))
			CreateLuaEvent(function() DelayMessage13(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 15) then
		if GetNameFromPublicHouses14(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords66(player:GetName()),GetCoords67(player:GetName()),GetCoords68(player:GetName()),GetCoords69(player:GetName())+2,GetCoords70(player:GetName()))
			CreateLuaEvent(function() DelayMessage14(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 16) then
		if GetNameFromPublicHouses15(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords71(player:GetName()),GetCoords72(player:GetName()),GetCoords73(player:GetName()),GetCoords74(player:GetName())+2,GetCoords75(player:GetName()))
			CreateLuaEvent(function() DelayMessage15(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 17) then
		if GetNameFromPublicHouses16(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords76(player:GetName()),GetCoords77(player:GetName()),GetCoords78(player:GetName()),GetCoords79(player:GetName())+2,GetCoords80(player:GetName()))
			CreateLuaEvent(function() DelayMessage16(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 18) then
		if GetNameFromPublicHouses17(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords81(player:GetName()),GetCoords82(player:GetName()),GetCoords83(player:GetName()),GetCoords84(player:GetName())+2,GetCoords85(player:GetName()))
			CreateLuaEvent(function() DelayMessage17(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 19) then
		if GetNameFromPublicHouses18(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords86(player:GetName()),GetCoords87(player:GetName()),GetCoords88(player:GetName()),GetCoords89(player:GetName())+2,GetCoords90(player:GetName()))
			CreateLuaEvent(function() DelayMessage18(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 20) then
		if GetNameFromPublicHouses19(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords91(player:GetName()),GetCoords92(player:GetName()),GetCoords93(player:GetName()),GetCoords94(player:GetName())+2,GetCoords95(player:GetName()))
			CreateLuaEvent(function() DelayMessage19(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 21) then
		if GetNameFromPublicHouses20(player:GetName()) == player:GetName() or player:GetName() == "Razorx" then
			player:Teleport(GetCoords96(player:GetName()),GetCoords97(player:GetName()),GetCoords98(player:GetName()),GetCoords99(player:GetName())+2,GetCoords100(player:GetName()))
			CreateLuaEvent(function() DelayMessage20(player); end, 3000, 1)
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your House!")
				player:GossipComplete()
			return
		end
	end
end

RegisterCreatureGossipEvent(TELEPORTER_ID, 1, GossipMenu)
RegisterCreatureGossipEvent(TELEPORTER_ID, 2, GossipSelect)


--< PUBLIC HOUSE TELEPORTER CODE