-- #############################
-- ##    Shared by RazorX14   ##
-- ##       For BotCore       ##
-- #############################


--> PUBLIC HOUSE VENDOR CODE

local VENDOR_ID = 80008
local ITEM_ID = 510016

-- Door GUID's
--local Door_01 = 217993
--local Door_02 = 217860
--local Door_03 = 217861
--local Door_04 = 217959
--local Door_05 = 217960
--local Door_06 = 217961
--local Door_07 = 217963
--local Door_08 = 217964
--local Door_09 = 217966
--local Door_10 = 217989
--local Door_11 = 217984
--local Door_12 = 217975
--local Door_13 = 217974
--local Door_14 = 217973
--local Door_15 = 217972
--local Door_16 = 217967
--local Door_17 = 217968
--local Door_18 = 217970
--local Door_19 = 217971
--local Door_20 = 217980

local Door_01 = 217860
local Door_02 = 217861
local Door_03 = 217959
local Door_04 = 217960
local Door_05 = 217961
local Door_06 = 217963
local Door_07 = 217964
local Door_08 = 217966
local Door_09 = 217975
local Door_10 = 217974
local Door_11 = 217973
local Door_12 = 217972
local Door_13 = 217967
local Door_14 = 217968
local Door_15 = 217970
local Door_16 = 217971

-- Kicker GUID's
--local Kicker_01 = 218092
--local Kicker_02 = 218093
--local Kicker_03 = 218094
--local Kicker_04 = 218095
--local Kicker_05 = 218096
--local Kicker_06 = 218097
--local Kicker_07 = 218098
--local Kicker_08 = 218099
--local Kicker_09 = 218100
--local Kicker_10 = 218101
--local Kicker_11 = 218102
--local Kicker_12 = 218103
--local Kicker_13 = 218104
--local Kicker_14 = 218105
--local Kicker_15 = 218106
--local Kicker_16 = 218107
--local Kicker_17 = 218108
--local Kicker_18 = 218109
--local Kicker_19 = 218110
--local Kicker_20 = 218111

local Kicker_01 = 218093
local Kicker_02 = 218094
local Kicker_03 = 218095
local Kicker_04 = 218096
local Kicker_05 = 218097
local Kicker_06 = 218098
local Kicker_07 = 218099
local Kicker_08 = 218100
local Kicker_09 = 218103
local Kicker_10 = 218104
local Kicker_11 = 218105
local Kicker_12 = 218106
local Kicker_13 = 218107
local Kicker_14 = 218108
local Kicker_15 = 218109
local Kicker_16 = 218110


--> DO NOT ALTER ANYTHING BELOW THIS POINT!!!

local function GetPUBLIC_COST(playername)
	local CHECK = WorldDBQuery("SELECT PUBLIC_COST from public_house_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetPUBLIC_SELL(playername)
	local CHECK = WorldDBQuery("SELECT PUBLIC_SELL from public_house_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end

local function GrantPublicHouse1(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='1' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_01 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_01 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=1")
	
end
local function GrantPublicHouse2(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='2' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_02 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_02 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=2")
end
local function GrantPublicHouse3(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='3' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_03 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_03 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=3")
end
local function GrantPublicHouse4(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='4' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_04 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_04 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=4")
end
local function GrantPublicHouse5(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='5' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_05 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_05 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=5")
end
local function GrantPublicHouse6(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='6' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_06 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_06 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=6")
end
local function GrantPublicHouse7(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='7' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_07 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_07 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=7")
end
local function GrantPublicHouse8(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='8' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_08 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_08 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=8")
end
local function GrantPublicHouse9(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='9' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_09 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_09 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=9")
end
local function GrantPublicHouse10(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='10' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_10 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_10 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=10")
end
local function GrantPublicHouse11(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='11' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_11 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_11 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=11")
end
local function GrantPublicHouse12(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='12' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_12 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_12 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=12")
end
local function GrantPublicHouse13(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='13' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_13 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_13 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=13")
end
local function GrantPublicHouse14(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='14' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_14 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_14 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=14")
end
local function GrantPublicHouse15(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='15' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_15 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_15 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=15")
end
local function GrantPublicHouse16(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='16' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_16 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_16 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=16")
end
local function GrantPublicHouse17(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='17' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_17 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_17 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=17")
end
local function GrantPublicHouse18(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='18' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_18 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_18 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=18")
end
local function GrantPublicHouse19(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='19' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_19 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_19 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=19")
end
local function GrantPublicHouse20(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`public_houses`(`Guid`) SELECT `characters`.`guid` FROM `characters` where guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET PublicHouse='20' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET DoorGuid=" .. Door_20 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses SET KickerGuid=" .. Kicker_20 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE public_houses_vendor_locs SET PlayerName='" .. playername .. "' where id=20")
end


local function Check_If_Bought1(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought2(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 2")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought3(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 3")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought4(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 4")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought5(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 5")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought6(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 6")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought7(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 7")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought8(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 8")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought9(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 9")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought10(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 10")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought11(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 11")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought12(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 12")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought13(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 13")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought14(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 14")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought15(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 15")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought16(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 16")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought17(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 17")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought18(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 18")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought19(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 19")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought20(playername)
	local CHECK = WorldDBQuery("SELECT PublicHouse from public_houses where PublicHouse = 20")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end


local function SellPublicHouse(playername, guid)
	local SPublicHouse = WorldDBQuery("DELETE from public_houses where PlayerName='" .. playername .. "'")
end


local function GetPublicHouse(playername)
local PublicHouse = WorldDBQuery("SELECT `PublicHouse` from `public_houses` where `PlayerName`='" .. playername .. "'")
	local house = 0
	if(PublicHouse) then
		repeat
			house = PublicHouse:GetUInt32(0)
		until not PublicHouse:NextRow()
	end
	return house
end


local function GetGuidFromPublicHouses(playername)
	local Guid = WorldDBQuery("SELECT `Guid` FROM `public_houses` where `PlayerName`='" .. playername .. "'")
    if(Guid) then
			guid = Guid:GetUInt32(0)
    return guid
	end
	return false
end


local function GetGUID(playername)
	local GUID = CharDBQuery("SELECT `guid` from `characters` where `name`='" .. playername .. "'")
	local id = 0
	if(GUID) then
        repeat
            id = GUID:GetUInt32(0)
        until not GUID:NextRow()
	end
    return id
end


local function On_GossipMenu(event, player, unit, sender, intid, code, popup, money)
	local PlrName = player:GetName()
	player:GossipMenuAddItem(0, "Public Houses", 0, 1, 0)
	player:GossipMenuAddItem(6, "Sell Public House", 0, 2000, 0, "Are You Sure You Wish To Sell?")
	player:GossipMenuAddItem(1, "Buy 15 [Solid Gold Coin]", 0, 8000, 0, "Buy 15 [Solid Gold Coin] For", "150000000")
	player:GossipSendMenu(1, unit)
end

local function GetGuildHouseOwner(playername)
	local GuildHouseOwner = WorldDBQuery("SELECT `PlayerName` FROM `guild_houses` where `PlayerName`='" .. playername .. "'")
    if(GuildHouseOwner) then
			GuildHouseOwner = GuildHouseOwner:GetString(0)
    return true
	end
	return false
end

local function GetNameFromPublicHouses1(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 1")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses2(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 2")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses3(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 3")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses4(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 4")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses5(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 5")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses6(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 6")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses7(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 7")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses8(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 8")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses9(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 9")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses10(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 10")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses11(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 11")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses12(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 12")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses13(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 13")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses14(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 14")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses15(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 15")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses16(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 16")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses17(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 17")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses18(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 18")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses19(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 19")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end
local function GetNameFromPublicHouses20(playername)
	local Guild = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` WHERE `PublicHouse` = 20")
    if(Guild) then
			guild = Guild:GetString(0)
    return "Sold"
	end
	return "Unsold"
end

local function On_GossipSelect(event, player, unit, sender, intid, code, popup, money)
		local PlrName = player:GetName()
	if (intid == 2000) then		
		if GetPublicHouse(PlrName) == 1 then
			if Check_If_Bought1(player:GetName()) == 1 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 1")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 2 then
			if Check_If_Bought2(player:GetName()) == 2 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 2")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 3 then
			if Check_If_Bought3(player:GetName()) == 3 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 3")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 4 then
			if Check_If_Bought4(player:GetName()) == 4 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 4")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 5 then
			if Check_If_Bought5(player:GetName()) == 5 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 5")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 6 then
			if Check_If_Bought6(player:GetName()) == 6 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 6")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 7 then
			if Check_If_Bought7(player:GetName()) == 7 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 7")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 8 then
			if Check_If_Bought8(player:GetName()) == 8 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 8")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 9 then
			if Check_If_Bought9(player:GetName()) == 9 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 9")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 10 then
			if Check_If_Bought10(player:GetName()) == 10 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 10")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 11 then
			if Check_If_Bought11(player:GetName()) == 11 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 11")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 12 then
			if Check_If_Bought12(player:GetName()) == 12 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 12")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 13 then
			if Check_If_Bought13(player:GetName()) == 13 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 13")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 14 then
			if Check_If_Bought14(player:GetName()) == 14 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 14")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 15 then
			if Check_If_Bought15(player:GetName()) == 15 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 15")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 16 then
			if Check_If_Bought16(player:GetName()) == 16 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 16")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 17 then
			if Check_If_Bought17(player:GetName()) == 17 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 17")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 18 then
			if Check_If_Bought18(player:GetName()) == 18 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 18")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 19 then
			if Check_If_Bought19(player:GetName()) == 19 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 19")
			player:GossipComplete()
			return
			end
		end
		if GetPublicHouse(PlrName) == 20 then
			if Check_If_Bought20(player:GetName()) == 20 then
			player:SendNotification("Your House Has Now Been Sold "..PlrName.."")
			SellPublicHouse(player:GetName())
			player:AddItem(ITEM_ID, GetPUBLIC_SELL(player:GetName()))
			SendWorldMessage(""..player:GetName().. " Has Just Sold A Public House..")
			WorldDBQuery("UPDATE `public_houses_vendor_locs` SET PlayerName = NULL WHERE `id` = 20")
			player:GossipComplete()
			return
			end
		else
			player:SendNotification("You Do Not Own A House "..PlrName.."")
			player:GossipComplete()
			return
		end
		if player:HasTitle(3202) then
			player:UnsetKnownTitle(3202)  -- Remove Homeowner Title
			player:ModifyMoney(100000)
			player:GossipComplete()
		end
	end
	if(intid == 1) then
	if GetGuidFromPublicHouses(player:GetName()) then
		player:SendNotification("You Already Have A House "..PlrName.."")
		player:GossipComplete()
		return
	end
	if GetGuildHouseOwner(player:GetName()) then
		player:SendNotification("You Cannot Own A GuildHouse And A PublicHouse "..PlrName.."")
		player:GossipComplete()
		return
	end
		player:GossipMenuAddItem(1, "[01] "..GetNameFromPublicHouses1(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 3, 0)
		player:GossipMenuAddItem(1, "[02] "..GetNameFromPublicHouses2(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 4, 0)
		player:GossipMenuAddItem(1, "[03] "..GetNameFromPublicHouses3(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 5, 0)
		player:GossipMenuAddItem(1, "[04] "..GetNameFromPublicHouses4(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 6, 0)
		player:GossipMenuAddItem(1, "[05] "..GetNameFromPublicHouses5(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 7, 0)
		player:GossipMenuAddItem(1, "[06] "..GetNameFromPublicHouses6(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 8, 0)
		player:GossipMenuAddItem(1, "[07] "..GetNameFromPublicHouses7(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 9, 0)
		player:GossipMenuAddItem(1, "[08] "..GetNameFromPublicHouses8(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 10, 0)
		player:GossipMenuAddItem(1, "[09] "..GetNameFromPublicHouses9(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 11, 0)
		player:GossipMenuAddItem(1, "[10] "..GetNameFromPublicHouses10(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 12, 0)
		player:GossipMenuAddItem(1, "[11] "..GetNameFromPublicHouses11(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 13, 0)
		player:GossipMenuAddItem(1, "[12] "..GetNameFromPublicHouses12(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 14, 0)
		player:GossipMenuAddItem(1, "[13] "..GetNameFromPublicHouses13(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 15, 0)
		player:GossipMenuAddItem(1, "[14] "..GetNameFromPublicHouses14(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 16, 0)
		player:GossipMenuAddItem(1, "[15] "..GetNameFromPublicHouses15(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 17, 0)
		player:GossipMenuAddItem(1, "[16] "..GetNameFromPublicHouses16(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 18, 0)
		--player:GossipMenuAddItem(1, "[17] "..GetNameFromPublicHouses17(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 19, 0)
		--player:GossipMenuAddItem(1, "[18] "..GetNameFromPublicHouses18(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 20, 0)
		--player:GossipMenuAddItem(1, "[19] "..GetNameFromPublicHouses19(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 21, 0)
		--player:GossipMenuAddItem(1, "[20] "..GetNameFromPublicHouses20(player:GetName()).." Public House, "..GetPUBLIC_COST(player:GetName()).." Coins", 0, 22, 0)		
		player:GossipMenuAddItem(0, "Back", 0, 1000)
		player:GossipSendMenu(1, unit)
	elseif(intid == 1000) then
		player:GossipMenuAddItem(0, "Public Houses", 0, 1, 0)
		player:GossipMenuAddItem(6, "Sell Public House", 0, 2000, 0, "Are You Sure You Wish To Sell?")
		player:GossipMenuAddItem(1, "Buy 15 [Solid Gold Coin]", 0, 8000, 0, "Buy [Solid Gold Coin] For", "150000000")
		player:GossipSendMenu(1, unit)
	elseif(intid == 8000) then
	if (player:GetCoinage() >= 150000000) then
		player:SendAreaTriggerMessage("You Have Bought a 15 [Solid Gold Coin]")
		player:AddItem(ITEM_ID, 15)
		player:ModifyMoney(-150000000)
		player:GossipComplete()
		return
		else
		player:SendAreaTriggerMessage("You Dont Have Enough Money")
		player:GossipComplete()
		return
		end
	elseif (intid == 3) then
		if Check_If_Bought1(player:GetName()) >= 1 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 1")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse1(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 4) then
		if Check_If_Bought2(player:GetName()) >= 2 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 2")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse2(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 5) then
		if Check_If_Bought3(player:GetName()) >= 3 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 3")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse3(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 6) then
		if Check_If_Bought4(player:GetName()) >= 4 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 4")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse4(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 7) then
		if Check_If_Bought5(player:GetName()) >= 5 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 5")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse5(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 8) then
		if Check_If_Bought6(player:GetName()) >= 6 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 6")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse6(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 9) then
		if Check_If_Bought7(player:GetName()) >= 7 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 7")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse7(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 10) then
		if Check_If_Bought8(player:GetName()) >= 8 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 8")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse8(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 11) then
		if Check_If_Bought9(player:GetName()) >= 9 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 9")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse9(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 12) then
		if Check_If_Bought10(player:GetName()) >= 10 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 10")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse10(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 13) then
		if Check_If_Bought11(player:GetName()) >= 11 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 11")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse11(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 14) then
		if Check_If_Bought12(player:GetName()) >= 12 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 12")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse12(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 15) then
		if Check_If_Bought13(player:GetName()) >= 13 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 13")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse13(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 16) then
		if Check_If_Bought14(player:GetName()) >= 14 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 14")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse14(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 17) then
		if Check_If_Bought15(player:GetName()) >= 15 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 15")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse15(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 18) then
		if Check_If_Bought16(player:GetName()) >= 16 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 16")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse16(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 19) then
		if Check_If_Bought17(player:GetName()) >= 17 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 17")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse17(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 20) then
		if Check_If_Bought18(player:GetName()) >= 18 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 18")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse18(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 21) then
		if Check_If_Bought19(player:GetName()) >= 19 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 19")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse19(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 22) then
		if Check_If_Bought20(player:GetName()) >= 20 then
			player:SendNotification("Someone Owns This House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This House "..PlrName.."")
			SendWorldMessage(""..player:GetName().. " Has Just Bought Public House Number 20")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetPUBLIC_COST(player:GetName()))
			GrantPublicHouse20(PlrName, TargetGUID)
			player:GossipComplete()
			return
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetPUBLIC_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	end
end

RegisterCreatureGossipEvent(VENDOR_ID, 1, On_GossipMenu)
RegisterCreatureGossipEvent(VENDOR_ID, 2, On_GossipSelect)


--< PUBLIC HOUSE VENDOR CODE