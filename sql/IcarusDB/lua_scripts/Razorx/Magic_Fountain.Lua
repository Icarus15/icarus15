function customObj_GossipMenu(event, player, object)
		player:GossipClearMenu()
		player:GossipMenuAddItem(0, "Heal Me", 0, 1)
		player:GossipSendMenu(1, object)
end

RegisterGameObjectGossipEvent(4002176, 1, customObj_GossipMenu)

function customObj_OnSelect(event, player, object, sender, intid, code)
	local clicked = object:GetGUIDLow()
	if clicked and (intid == 1) then
		player:SendAreaTriggerMessage("You are now fully healed!")
		player:CastSpell(player, 69693, true)
		local aura = player:GetAura(15007)
        if(aura) then
            aura:Remove()
        end
		player:GossipComplete()
	end
end

RegisterGameObjectGossipEvent(4002176, 2, customObj_OnSelect)