#include "ScriptPCH.h"
#include "Chat.h"

class Kill_You : public PlayerScript
{
	public:
		Kill_You() : PlayerScript("Kill_You") { }

		void OnPVPKill(Player * killer, Player * victim)

	{
			if (killer->GetGUID() == victim->GetGUID())
				return;
			{
				killer->ModifyMoney(killer->GetMoney() + 1000);
				killer->AddItem(11966, 1);
				killer->SetHealth(killer->GetMaxHealth());
				killer->SetPower(POWER_MANA, killer->GetMaxPower(POWER_MANA));
				killer->SetPower(POWER_RAGE, killer->GetMaxPower(POWER_RAGE));
				killer->SetPower(POWER_ENERGY, killer->GetMaxPower(POWER_ENERGY));
				killer->SetPower(POWER_RUNIC_POWER, killer->GetMaxPower(POWER_RUNIC_POWER));
				killer->SetReputation(369, killer->GetReputation(369) +1);
				killer->SetReputation(470, killer->GetReputation(470) +1);
				killer->SetReputation(21, killer->GetReputation(21) +1);		
				killer->CastSpell(killer, 4987, true);
				killer->GetSession()->SendNotification("Nice job! I bet you feel cool now for crushing that poor bastard!");
				victim->GetSession()->SendNotification("You got knocked the Fuck out!!!");
			}
		}

};

void AddSC_Kill_You()
{
	new Kill_You();
}