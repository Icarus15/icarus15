class npc_xp : public CreatureScript
{
public:
	npc_xp() : CreatureScript("npc_xp") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I no longer wish to gain experience.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I wish to start gaining experience again.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		if (player->IsInCombat())
		{
			player->CLOSE_GOSSIP_MENU();
			creature->MonsterWhisper("You're in combat", player);
			return true;
		}

		if (sender == GOSSIP_SENDER_MAIN)
		{
			switch(actions)
			{
			case GOSSIP_ACTION_INFO_DEF+1:
				{
					player->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_NO_XP_GAIN);
					creature->MonsterWhisper("You will no longer gain experience.", player);
				}
				break;

			case GOSSIP_ACTION_INFO_DEF+2:
				{
					player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_NO_XP_GAIN);
					creature->MonsterWhisper("You will gain experience again.", player);
				}
				break;

			case GOSSIP_ACTION_INFO_DEF+3:
				{
					player->PlayerTalkClass->SendCloseGossip();
				}
				break;
			}
		}

		return true;
	}
};

void AddSC_npc_xp()
{
	new npc_xp;
}