/*
* GOSSIP_ICON_CHAT                = 0      White chat bubble
* GOSSIP_ICON_VENDOR              = 1      Brown bag
* GOSSIP_ICON_TAXI                = 2      Flight
* GOSSIP_ICON_TRAINER             = 3      Book
* GOSSIP_ICON_INTERACT_1          = 4      Interaction wheel
* GOSSIP_ICON_INTERACT_2          = 5      Interaction wheel
* GOSSIP_ICON_MONEY_BAG           = 6      Brown bag with yellow dot
* GOSSIP_ICON_TALK                = 7      White chat bubble with black dots
* GOSSIP_ICON_TABARD              = 8      Tabard
* GOSSIP_ICON_BATTLE              = 9      Two swords
* GOSSIP_ICON_DOT                 = 10     Yellow dot
* GOSSIP_ICON_CHAT_11             = 11,    White chat bubble
* GOSSIP_ICON_CHAT_12             = 12,    White chat bubble
* GOSSIP_ICON_CHAT_13             = 13,    White chat bubble
* GOSSIP_ICON_UNK_14              = 14,    INVALID - DO NOT USE
* GOSSIP_ICON_UNK_15              = 15,    INVALID - DO NOT USE
* GOSSIP_ICON_CHAT_16             = 16,    White chat bubble
* GOSSIP_ICON_CHAT_17             = 17,    White chat bubble
* GOSSIP_ICON_CHAT_18             = 18,    White chat bubble
* GOSSIP_ICON_CHAT_19             = 19,    White chat bubble
* GOSSIP_ICON_CHAT_20             = 20,    White chat bubble
*/

#include "ScriptPCH.h"
#include "Config.h"

#define EMOTE_FUNCTION_OFF "Function Disable!"

enum MenuStructure
{
	MAIN_MENU      		= 1,
	KARA_MENU     		= 2,
};

enum Actions
{
	ACTION_LETS_GO		                 = 10, 
	ACTION_NOT_YET  	                 = 11,
};

class npc_karatelone : public CreatureScript
{
public:
	npc_karatelone() : CreatureScript("npc_karatelone") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();

		if (sConfigMgr->GetBoolDefault("Server.Karatel.Main.Enable", true))
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "Take me to the Wizards ->", GOSSIP_SENDER_MAIN, KARA_MENU);
			player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
			return true;
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{
		case KARA_MENU:
			if (sConfigMgr->GetBoolDefault("Server.Karatel.One.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "Lets Go! I'm ready.", GOSSIP_SENDER_MAIN, ACTION_LETS_GO);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "Not Now! I'm not ready!", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;

		case ACTION_LETS_GO:
			player->CLOSE_GOSSIP_MENU();
			player->TeleportTo(532, -11172.4f, -1906.39f, 165.766f, 2.194f);
			break;

		case ACTION_NOT_YET:
			player->CLOSE_GOSSIP_MENU();
			break;
		
		case MAIN_MENU:
			OnGossipHello(player, creature);
			break;
		}
		return true;
	}
};

void AddSC_npc_karatelone()
{
	new npc_karatelone();
}