/*
Custom Fly All Command
Written by Josh Carter
Copyright Icarus Gaming 2013-2016 all rights reserved.
For Soul Thief 255 server
*/
#include "Chat.h"
#include "Config.h"
#include "Group.h"
#include "Language.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Unit.h"
#include "RBAC.h"

class script_fly_everywhere_commands : public CommandScript
{
public:
    script_fly_everywhere_commands() : CommandScript("script_fly_everywhere_commands") { }

    ChatCommand* GetCommands() const
    {
        static ChatCommand flyallCommandTable[] =
        {
            { "on",		rbac::RBAC_PERM_COMMAND_FLYALL_ON,  false, &HandleFlyAllOnCommand,  "", NULL },
			{ "off",	rbac::RBAC_PERM_COMMAND_FLYALL_OFF, false, &HandleFlyAllOffCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }	
		};
		static ChatCommand commandTable[] =
        {
            { "flyall",	rbac::RBAC_PERM_COMMAND_FLYALL,		false, NULL,					"", flyallCommandTable },
            { NULL, 0, false, NULL, "", NULL }
		};

		return commandTable;
	}

	static bool HandleFlyAllOnCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();
        handler->SetSentErrorMessage(true);
		if (player->IsInCombat() ||
			player->IsInFlight() ||
			player->IsCharmed() ||
			player->isAttackingPlayer() ||
			player->isFeared() ||
			player->isFrozen() ||
			player->IsOutdoorPvPActive() ||
			player->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP) ||
			/* Disabled In WinterGrasp */
			player->GetAreaId() == 4197 ||
			/* Disabled for Arena Ball */
			player->GetAreaId() == 6823 ||
			/* Disabled for GoldRush BG */
			player->GetAreaId() == 6665 ||
			player->GetAreaId() == 6753 ||
			player->GetAreaId() == 6736 ||
			player->GetAreaId() == 6754 ||
			player->GetAreaId() == 6737)
			{
			handler->SendSysMessage("You cannot summon your mount right now");
			return false;
			}	
			else 
			{
				player->CastSpell(player, 31700, true);
			}
			return true;
	}
	
	static bool HandleFlyAllOffCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();
        handler->SetSentErrorMessage(true);
        if (player->IsInCombat() ||
            player->IsInFlight() ||
            player->IsCharmed()  ||
			player->IsFlying())
		{
			handler->SendSysMessage("You cannot dismount here!");
			return false;
			}	
			else 
			{
				player->RemoveAura(31700);
			}
			return true;
	}
};

void AddSC_script_fly_everywhere_commands()
{
    new script_fly_everywhere_commands();
}
