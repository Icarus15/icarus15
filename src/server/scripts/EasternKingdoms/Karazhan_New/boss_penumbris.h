#include "ScriptPCH.h"
using namespace std;

/* Player */
uint64 p_PlayerGUID;
string splayerName;
/* Is Active */
bool IsBattleActive = false;
bool HasLogged = false;
bool PlayerInZone = true;
bool IsBossDead = false;

enum SpawnIds
{
	/* First Wave */
	NPC_PORTAL =             666555, // Need new ID's after creation
	NPC_QUEST_GIVER =        666556,
};

enum WhereTel
{
	KARAZHAN_ZONE = 3457,
	KARAZHAN_AREA = 3457,
};

#define MAX_WAVE_SPAWN_LOCATIONS 2
const uint32 summonList[MAX_WAVE_SPAWN_LOCATIONS] =
{
	NPC_PORTAL, // Position 0 Coords
	NPC_QUEST_GIVER, // Position 1 Coords
};

// Spawn these on Death
static Position a_NpcSpawn[] =
{
	/*    X               Y            Z           O      */
	/* NPC Spawn Location */
	{ -10920.942f, -2079.818f, 92.174f, 3.983f }, //Portal NPC Location
};

static Position b_NpcSpawn[] =
{
	/*    X               Y            Z           O      */
	/* NPC Spawn Location */
	{ -10926.673f, -2075.255f, 92.173f, 3.986f }, //Quest NPC Location
};

//despawn npcs
void DoBattleCleanup(Creature * me, SummonList summons)
{
	IsBattleActive = true;
	//isBossDead = false;
	summons.DespawnAll();
	me->DespawnOrUnsummon(1);
}
