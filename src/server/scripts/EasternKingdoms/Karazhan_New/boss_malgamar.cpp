/*
 * Copyright (C) 2013 Project Icarus <http://icarusgaming.org/>
 * Scripted by JoshCarter
 * For Custom Karazhan Raid.
 */

/* ScriptData
SDCreator: JCarter
SDName: boss_malgamar
SD%Complete: 90
SDComment: Needs Tested
EndScriptData */

#include "ScriptPCH.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

#define EMOTE_ONAGGRO      "Kill him quickly heros or you shall all perish!" 
#define EMOTE_STRENGTH     "Mal'gamar is increasing in strenth!"  
#define EMOTE_FAIL         "Mal'gamar begins to cast beams of light from his fingertips! We are all lost..." 
#define EMOTE_WIN          "We did it! A great victory has been won today!"

enum Spells
{
	//Phase one
	SPELL_TWILIGHT_CHANNEL             = 57797,
	SPELL_TWILIGHT_PORTAL_VISUAL       = 51807,
	//Phase two  
    SPELL_AURA_OF_DREAD                = 41142,
	SPELL_SUNDER_ARMOR                 = 59350,
    SPELL_UNHOLY_POWER                 = 69167,
	SPELL_BURNING_FISTS                = 67333,
	SPELL_PAIN_AND_SUFFERING           = 73790,
    SPELL_FIERY_COMBUSTION             = 74562,
	SPELL_MARK_OF_COMBUSTION           = 74567,
	//Phase three 
	SPELL_HIGH_SECURITY_SHADOW_PRISON  = 45922,
	SPELL_FINGER_OF_DEATH              = 31984,
	SPELL_BERSERK                      = 41924,
	SPELL_SINBEAM					   = 40827,	
};

enum Creatures  /*MALGAMAR 5000005*/
{
    NPC_ANCIENT_WISP_ONE     = 5000006,
	NPC_TWILIGHT_PORTAL      = 5000007,
	NPC_ANCIENT_WISP_TWO     = 5000008,
};

enum Yells
{
	SOUND_ONAGGRO               = 0,
	SOUND_ONSLAY                = 1,
	SOUND_ONDEATH               = 2,
};

class boss_malgamar : public CreatureScript
{
public:
	boss_malgamar() : CreatureScript("boss_malgamar"){ }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
        return new boss_malgamarAI (creature);
    }

    struct boss_malgamarAI : public ScriptedAI
	{
		
		boss_malgamarAI(Creature* creature) : ScriptedAI(creature) { }
		
		
		uint32 ChannelTwilight_Timer;

		uint32 AuraOfDread_Timer;
        uint32 SunderArmor_Timer;
		uint32 UnholyPower_Timer;
        uint32 BurningFists_Timer;
        uint32 PainAndSuffering_Timer;
		uint32 FieryCombustion_Timer;
      
		uint32 Enrage_Timer;
		uint32 FingerOfDeath_Timer;

        bool IsChanneling;
        bool Enraged;
					
		void Reset()
		{
			IsChanneling = false;
            Enraged = false;
            ChannelTwilight_Timer = 0;

			AuraOfDread_Timer = 20000;
            SunderArmor_Timer = 10000;
			UnholyPower_Timer = 9750;
            BurningFists_Timer = 10000;
            PainAndSuffering_Timer = urand(10000, 17500);
			FieryCombustion_Timer = urand(7500, 15000);

			FingerOfDeath_Timer = 1;
            Enrage_Timer = 600000;
			
			me->RemoveAllAuras(); 
            me->SetFullHealth();
						
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 50966); // Staff Abracadaver
		}

		void JustReachedHome()
        {
            DoCast(me, SPELL_TWILIGHT_CHANNEL);
        }
		
		void EnterCombat(Unit* who)
		{
		    me->MonsterYell("You dare interrupt me!!!", LANG_UNIVERSAL, me);
            Talk(SOUND_ONAGGRO);
            me->MonsterTextEmote(EMOTE_ONAGGRO, 0, true);
            me->InterruptSpell(CURRENT_CHANNELED_SPELL);
            me->RemoveAllAuras(); 
            DoZoneInCombat();
		}

		void KilledUnit(Unit* /*who*/)
		{
		    Talk(SOUND_ONSLAY);
		}
		
		void JustDied(Unit* /*who*/)
		{
		    me->MonsterYell("I have.. failed...", LANG_UNIVERSAL, me);
            Talk(SOUND_ONDEATH);
            me->RemoveAllAuras();
			me->MonsterTextEmote(EMOTE_WIN, 0, true);
			}
		
		void UpdateAI(uint32 diff)
        {
            if (ChannelTwilight_Timer <= diff)
            {
                if (!IsChanneling)
                {
                    DoCast(me, SPELL_TWILIGHT_CHANNEL);
                    IsChanneling = true;
                }
            } else ChannelTwilight_Timer -= diff;    
		
			if(!UpdateVictim())
				return;
			
			if (Enrage_Timer < diff && !Enraged)
            {
                me->GetMotionMaster()->Clear(false);
                me->GetMotionMaster()->MoveIdle();
                me->RemoveAllAuras(); 
                me->MonsterTextEmote(EMOTE_FAIL, 0, true);
                Enraged = true;
            } else Enrage_Timer -= diff;

            if (Enraged)
            {
                DoCast(me, SPELL_BERSERK);

				if (FingerOfDeath_Timer <= diff)
                {
                    if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
			        DoCast (Target, SPELL_SINBEAM);
                    FingerOfDeath_Timer = 1000;
                } else FingerOfDeath_Timer -= diff;
                return; 
			}
			
			if (AuraOfDread_Timer <= diff)
		        {
			        DoCast(me->GetVictim(), SPELL_AURA_OF_DREAD);
			        AuraOfDread_Timer = 20000;
		        } else AuraOfDread_Timer -= diff;

		        if (SunderArmor_Timer <= diff)
		        {
			        DoCast(me->GetVictim(), SPELL_SUNDER_ARMOR);
			        SunderArmor_Timer = 25000;
                } else SunderArmor_Timer -= diff;

		        if (BurningFists_Timer <= diff)
		        {
			        DoCast(me->GetVictim(), SPELL_BURNING_FISTS);
			        BurningFists_Timer = 35000;
                } else BurningFists_Timer -= diff;

		    	if (UnholyPower_Timer <= diff)
		        {
		        	DoCast (me->GetVictim(), SPELL_UNHOLY_POWER);
		        	UnholyPower_Timer = 25000;
		            me->MonsterTextEmote(EMOTE_STRENGTH, 0, true);
                } else UnholyPower_Timer -= diff;

		        if (PainAndSuffering_Timer <= diff)
		        {
                    if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
			        DoCast(Target, SPELL_PAIN_AND_SUFFERING);
			        PainAndSuffering_Timer = urand(10000, 17500);
		        } else PainAndSuffering_Timer -= diff;
 
			    if (FieryCombustion_Timer <= diff)
		        {
                    if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
			        DoCast(Target, SPELL_FIERY_COMBUSTION);
			        FieryCombustion_Timer = urand(12500, 20000);      
		        } else FieryCombustion_Timer -= diff;			

            DoMeleeAttackIfReady();
        }
    };
};

class npc_fire_wisp_portal : public CreatureScript
{
public:
    npc_fire_wisp_portal() : CreatureScript("npc_fire_wisp_portal") { }

    CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
        return new npc_fire_wisp_portalAI(creature);
	}

    struct npc_fire_wisp_portalAI : public ScriptedAI
    {
		npc_fire_wisp_portalAI(Creature* creature) : ScriptedAI(creature){ }

		void Reset()
        {
			DoCast(me, SPELL_TWILIGHT_PORTAL_VISUAL);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        }

		void UpdateAI(uint32 diff)
        {
			if (!UpdateVictim())
				return;
        }
	};
};

void AddSC_boss_malgamar()
{
    new boss_malgamar();
	new npc_fire_wisp_portal();
}