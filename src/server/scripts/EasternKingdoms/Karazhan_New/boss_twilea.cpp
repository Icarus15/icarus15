/*
Boss_Twilea
Custom Karazhan Boss
Icarus Gaming Inc
By: Johnathan J. Carter
And Matthew Ferrill
*/

/* 
ScriptData
SDCategory: Karazhan_New
SDName: Boss Twilea
SD%Complete: 95
SDComment: Needs in game test.
EndScriptData 
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_twilea.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

class boss_twilea : public CreatureScript
{
public:
	boss_twilea() : CreatureScript("boss_twilea") { }

	struct boss_twileaAI : public ScriptedAI
	{
		boss_twileaAI(Creature * c) : ScriptedAI(c) { }
	
		uint32 uiEnrageTimer;
		uint32 uiWebsprayTimer;
		uint32 uiSinBeamTimer;
		bool ShadowStorm;
		bool WaveOne;
		bool WaveTwo;
		bool WaveThree;
		bool WaveFour;
		bool WaveFive;
		bool WaveSix;
		bool WaveSeven;		
		bool WaveEight;
		bool Enraged;
		bool AggroSummon;
		
		void Reset()
		{
		uiWebsprayTimer = 45000;
		uiEnrageTimer = 900000; 	
		uiSinBeamTimer = 20000;
		ShadowStorm	= true;
		WaveOne	= true;
		WaveTwo	= false;
		WaveThree = false;
		WaveFour = false;
		WaveFive = false;
		WaveSix = false;
		WaveSeven = false;
		WaveEight = false;
		Enraged = false;
		AggroSummon = false;
		me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 40489); // Great Staff of the Nexus
		me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, UNIT_BYTE1_FLAG_ALWAYS_STAND);
		}
		
		void JustDied(Unit* /*killer*/) OVERRIDE
		{
		Talk(SAY_BOSS_DEATH);
		}
		
		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
		Talk(SAY_AGGRO);
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
		Talk(SAY_PLAYER_DEATH);
		}	

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;	

			if (uiEnrageTimer <=diff && !Enraged)
			{
				DoCast(me, SPELL_FRENZY, true);
				me->MonsterYell("I grow tired of this.... ", LANG_UNIVERSAL, me);
				me->MonsterYell("From the deepest depths of my lair, I call you, Huirato, Guirato, come forth!!!!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[10], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(T_waveList[11], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
			Enraged = true;
			}
		
			if(HealthBelowPct(100) && ShadowStorm) 
			{
				me->AddAura(SPELL_SHADOW_STORM, me);
				ShadowStorm = false;
			}
		
			if(WaveOne && HealthBelowPct(90))
			{
				me->MonsterYell("You cannot truly hope to defeat me, come forth my pets, save your master!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[0], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(T_waveList[1], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveOne = false;
				WaveTwo = true;
			}
			
			if(WaveTwo && HealthBelowPct(80))
			{
				me->MonsterYell("You cannot truly hope to defeat me, come forth my pets, save your master!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[0], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(T_waveList[1], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveTwo = false;
				WaveThree = true;
			}
		
			if(WaveThree && HealthBelowPct(70)) 
			{
				me->MonsterYell("I refuse to give in to the likes of you, come forth my pets, save your master!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[2], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(T_waveList[3], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveThree = false;
				WaveFour = true;
			}	
		
			if(WaveFour && HealthBelowPct(60))
			{
				me->MonsterYell("My poor little creatures...", LANG_UNIVERSAL, me);
				me->MonsterYell("Their tiny bodies scattered everywhere...", LANG_UNIVERSAL, me);
				me->MonsterYell("Come forth my pets, and avenge your brothers and sisters!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[2], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
			me->SummonCreature(T_waveList[3], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveFour = false;
				WaveFive = true;
			}	
		
			if(WaveFive && HealthBelowPct(50))
			{
				me->MonsterYell("Now your making me angry, flee while you can...", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[4], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->MonsterYell("Minions, chase them away from here!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[5], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveFive = false;
				WaveSix = true;
			}			
		
			if(WaveSix && HealthBelowPct(40))
			{
				me->MonsterYell("I will bathe in your blood and pick my teeth with your bones!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[4], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->MonsterYell("Minions come forth and destroy!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[5], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveSix = false;
				WaveSeven = true;
			}
		
			if(WaveSeven && HealthBelowPct(20))
			{
				me->MonsterYell("My poor heart... I think... it slows....!!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[6], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->MonsterYell("More powerful minions then to dispatch this rabble, Aid Your Queen!!", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[7], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveSeven = false;
				WaveEight = true;
			}
		
			if(WaveEight && HealthBelowPct(10))
			{
				me->MonsterSay("Oh....oh my... Look at all the little red puddles... drop by drop I fade away...", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[6], T_waveSpawns[0].m_positionX, T_waveSpawns[0].m_positionY, T_waveSpawns[0].m_positionZ, T_waveSpawns[0].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->MonsterSay("You're my last hope my lovely little spiders...  Help your queen...ahhh...", LANG_UNIVERSAL, me);
				me->SummonCreature(T_waveList[7], T_waveSpawns[1].m_positionX, T_waveSpawns[1].m_positionY, T_waveSpawns[1].m_positionZ, T_waveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				WaveEight = false;
			}
		
			if (uiWebsprayTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_WEB_SPRAY);
					me->MonsterYell("This will slow you down!!!", LANG_UNIVERSAL, me);
				}
				uiWebsprayTimer = 45000;
			} else	uiWebsprayTimer -= diff;			
		
			if (uiSinBeamTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_SINBEAM);
					me->MonsterYell("I call forth Blood Lightning to devour your flesh!!!", LANG_UNIVERSAL, me);
				}
				uiSinBeamTimer = urand(15000, 55000);
			} else	uiSinBeamTimer -= diff;	
		
		}
	};
		
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new boss_twileaAI(pCreature);
	}
};
		
class npc_t_portal : public CreatureScript 
{
public:
	npc_t_portal() : CreatureScript("npc_t_portal") { }

	struct npc_t_portalAI : public ScriptedAI
	{
		npc_t_portalAI(Creature * c) : ScriptedAI(c) { }

		void Reset()
		{
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
		}
	};
	
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_t_portalAI(pCreature);
	}
};			
		
class npc_scarab : public CreatureScript 
{
public:
	npc_scarab() : CreatureScript("npc_scarab") { }

	struct npc_scarabAI : public ScriptedAI
	{
		npc_scarabAI(Creature * c) : ScriptedAI(c) { }

		uint32 MeteorFistTimer;		
				
		void Reset()	
		{
			MeteorFistTimer = 1000;
		}
		
		void MoveInLineOfSight(Unit * twho)
		{
			if (twho->GetTypeId() != TYPEID_PLAYER)
				return;
			
			if (me->isAttackingPlayer())
				return;
			/* Test Above 2nd Term check to not pickup players each time they move */
			
			if (twho && me->IsWithinDistInMap(twho, 50.0f))
			{
				me->SetInCombatWith(twho);
				me->AI()->AttackStart(twho);
				me->GetMotionMaster()->MoveChase(twho, 50.0f); // Reduced pull dist
				me->MonsterYell("We hear your summons mistress and obey!!!!", LANG_UNIVERSAL, me);
			}	
		}
		
		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;	
	
			if(MeteorFistTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_METEOR_FISTS);
				me->MonsterYell("Meteor Strike!!!!", LANG_UNIVERSAL, me);
				MeteorFistTimer = urand(1000, 60000); 
			}
			else
				MeteorFistTimer -= diff;	
			DoMeleeAttackIfReady();
		}	
	};
	
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_scarabAI(pCreature);
	}
	
};		

class npc_scarab_risen : public CreatureScript 
{
public:
	npc_scarab_risen() : CreatureScript("npc_scarab_risen") { }

	struct npc_scarab_risenAI : public ScriptedAI
	{
		npc_scarab_risenAI(Creature * c) : ScriptedAI(c) { }

		uint32 MeteorFistTimer;		
		uint32 SoulReaperTimer;
		bool FrenzyTimer;
		
		void Reset()	
		{
			FrenzyTimer = true; 
			MeteorFistTimer = 10000;
			SoulReaperTimer = 20000;
		}
	
		void MoveInLineOfSight(Unit * twho)
		{
			if (twho->GetTypeId() != TYPEID_PLAYER)
				return;
			
			if (me->isAttackingPlayer())
				return;

			if (twho && me->IsWithinDistInMap(twho, 50.0f))
			{
				me->SetInCombatWith(twho);
				me->AI()->AttackStart(twho);
				me->GetMotionMaster()->MoveChase(twho, 50.0f);
			}	
		}
			
		void UpdateAI(uint32 diff) OVERRIDE
		{
		
			if(!UpdateVictim())
				return;	
		
			if(HealthBelowPct(100) && FrenzyTimer) 
			{
				DoCast(me, SPELL_FRENZY);
				FrenzyTimer = false;
			}
	
			if (MeteorFistTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_METEOR_FISTS);
					me->MonsterYell("Meteor Strike!!!", LANG_UNIVERSAL, me);
					MeteorFistTimer = urand(20000, 60000);
				}
				
			} else MeteorFistTimer -= diff;
			
			if (SoulReaperTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_SOUL_REAPER);
					me->MonsterYell("I will eat your soul!!!", LANG_UNIVERSAL, me);
					SoulReaperTimer = urand(10000, 40000);
				}
				
			} else SoulReaperTimer -= diff;
					DoMeleeAttackIfReady();
		}
	};
	
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_scarab_risenAI(pCreature);
	}
	
};

class npc_spider : public CreatureScript 
{
public:
	npc_spider() : CreatureScript("npc_spider") { }

	struct npc_spiderAI : public ScriptedAI
	{
		npc_spiderAI(Creature * c) : ScriptedAI(c) { }

		uint32 SpiderKissTimer;
		uint32 PoisonVolleyTimer;		
						
		void Reset()	
		{
			SpiderKissTimer = 10000; 
			PoisonVolleyTimer = 20000;
		}
	
		void EnterCombat(Unit* who) OVERRIDE
		{
			me->MonsterYell("I hear you call mistress, and obey!!!", LANG_UNIVERSAL, me);
		}
		/*Test Run*/
		void MoveInLineOfSight(Unit * twho)
		{
			if (twho->GetTypeId() != TYPEID_PLAYER)
				return;
			
			if (me->isAttackingPlayer())
				return;

			if (twho && me->IsWithinDistInMap(twho, 500.0f))
			{
				me->MonsterYell("We obey the Spider Queen!!!!", LANG_UNIVERSAL, me);
				me->SetInCombatWith(twho);
				me->AI()->AttackStart(twho);
				me->GetMotionMaster()->MoveChase(twho, 500.0f);
			}	
		}
		
		void UpdateAI(uint32 diff) OVERRIDE
		{
		
			if(!UpdateVictim())
				return;	
	
			if(SpiderKissTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SPIDER_KISS); 
				SpiderKissTimer = urand(20000, 40000);
			}
			else
				SpiderKissTimer -= diff;
	
			if (PoisonVolleyTimer <= diff)
				{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
					{
						DoCast(Target, SPELL_POISON_VOLLY);
						me->MonsterYell("Feel my crippling poison infect you!!!", LANG_UNIVERSAL, me);
						PoisonVolleyTimer = urand(35000, 50000);
					}
					
				} else PoisonVolleyTimer -= diff;
					DoMeleeAttackIfReady();
		}	
	};
	
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_spiderAI(pCreature);
	}
};		

class npc_large_spider : public CreatureScript 
{
public:
	npc_large_spider() : CreatureScript("npc_large_spider") { }

	struct npc_large_spiderAI : public ScriptedAI
	{
		npc_large_spiderAI(Creature * c) : ScriptedAI(c) { }

		uint32 SpiderKissTimer;
		uint32 PoisonVolleyTimer;
		bool FrenzyTimer;		
		
		void Reset()	
		{
			SpiderKissTimer = urand(20000, 40000);
			PoisonVolleyTimer = urand(30000, 60000);
			FrenzyTimer = true;
		}
	
		void EnterCombat(Unit* who) OVERRIDE
		{
			me->MonsterYell("I hear you call mistress, and obey!!!!", LANG_UNIVERSAL, me);
		}
		/* Test Run */
		void MoveInLineOfSight(Unit * twho)
		{
			if (twho->GetTypeId() != TYPEID_PLAYER)
				return;
			
			if (me->isAttackingPlayer())
				return;

			if (twho && me->IsWithinDistInMap(twho, 500.0f))
			{
				me->SetInCombatWith(twho);
				me->AI()->AttackStart(twho);
				me->GetMotionMaster()->MoveChase(twho, 500.0f);
			}	
		}
		
		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;	
	
			if(HealthBelowPct(100) && FrenzyTimer) 
			{
				DoCast(me, SPELL_FRENZY);
				FrenzyTimer = false;
			}
		
			if(SpiderKissTimer <= diff)
			{
			if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_SPIDER_KISS);
					me->MonsterYell("Hmmmm.... flesh so good!!!", LANG_UNIVERSAL, me);
					SpiderKissTimer = urand(20000, 40000);
				}	 
			else
				SpiderKissTimer -= diff;
			}
		
			if(PoisonVolleyTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_POISON_VOLLY);
					me->MonsterYell("Feel my deadly poison infect you!!!", LANG_UNIVERSAL, me);
					PoisonVolleyTimer = urand(30000, 60000);
				}	 
			else
				PoisonVolleyTimer -= diff;
					DoMeleeAttackIfReady();	
			
		}
	}
};
	
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_large_spiderAI(pCreature);
	}
};		

void AddSC_twilea_battle()
{
	new boss_twilea;
	new npc_t_portal;
	new npc_scarab;
	new npc_scarab_risen;
	new npc_spider;
	new npc_large_spider;
}