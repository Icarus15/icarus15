/*
**********************************
*      BotCore Custom Boss       *
*   	   Boss Naxril 			 *
*      Type: Boss Encounter		 *
*         By Josh Carter         *
**********************************
*/

/* ScriptData
SDCategory: Karazhan
SDName: Boss_Naxril
SD%Complete: 100%
SDComment: Needs in game test
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_wizards.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum Yells
{
	SAY_AGGRO									= 0, // 0 == Group ID for Creature_Text Table -> Sound included.
	SAY_PATHETIC								= 1,
	SAY_SLAY									= 2,
};

class boss_naxril : public CreatureScript
{
public:
	boss_naxril() : CreatureScript("boss_naxril") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_naxrilAI (creature);
	}

	struct boss_naxrilAI : public ScriptedAI
	{
		boss_naxrilAI(Creature* creature) : ScriptedAI(creature) {}

		uint32 MeteorstrikeTimer;
		uint32 JumpattackTimer;
		uint32 DeathgripTimer;
		uint32 EnrageTimer;

		void Reset()
		{
			MeteorstrikeTimer		= urand(15000, 65000);
			JumpattackTimer		  	= urand(45000, 85000);
			DeathgripTimer			= urand(75000, 95000);
			EnrageTimer				= 600000;	// 10 min Enrage Timer
		}

		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			Talk(SAY_PATHETIC);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			Talk(SAY_SLAY);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			Talk(SAY_AGGRO);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if (!UpdateVictim())
				return;

			if (EnrageTimer <=diff && !EnrageTimer)
			{
				DoCast(me, SPELL_FRENZY);
				DoCast(me, SPELL_MASTER_BUFF_MELEE);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC);
				DoCast(me, SPELL_ASHBRINGER_FURY);
			}
			else
				EnrageTimer -= diff;

			if (MeteorstrikeTimer <= diff)
			{
				DoCastVictim(SPELL_METEOR_STRIKE);
				me->MonsterYell("I Squish you all!!!", LANG_UNIVERSAL, me);
				MeteorstrikeTimer = urand(15000, 65000);
			} else MeteorstrikeTimer -= diff;

			if (JumpattackTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
					DoCast(Target,SPELL_JUMP_ATTACK);
				DoCastVictim(SPELL_METEOR_FISTS);
				me->MonsterYell("I Squish this one!!!", LANG_UNIVERSAL, me); // Look up in creature text for sound ID
				JumpattackTimer = urand(45000, 85000);
			} else JumpattackTimer -= diff;

			if (DeathgripTimer <= diff)
			{
				DoCastVictim(SPELL_DEATH_GRIP, true);
				me->MonsterYell("I Choke this one, HAHAHAHA!!!!", LANG_UNIVERSAL, me);
				DeathgripTimer = urand(75000, 95000);
			} else DeathgripTimer -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_naxril()
{
	new boss_naxril();
}