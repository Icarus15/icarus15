/*
* Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2013-2015
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KARAZHAN_H
#define DEF_KARAZHAN_H

enum DataTypes
{
	TYPE_GIZELLA					= 1,
	TYPE_ERITHOL					= 2,
	TYPE_PENUMBRIS					= 3,
	TYPE_RUARZOG					= 4,
	TYPE_NAXRIL						= 5,
	TYPE_WIXAR						= 6,
	TYPE_MORTUM						= 7,
	TYPE_LUES						= 8,
	TYPE_GLACIA						= 9,
	TYPE_FIEROS						= 10,
	TYPE_MALGAMAR					= 11,
	TYPE_MIRXAL						= 12,
};

#define ERROR_INST_DATA(a)          TC_LOG_ERROR(LOG_FILTER_TSCR, "Instance Data for Karazhan not set properly. Encounter for Creature Entry %u may not work properly.", a->GetEntry());
#endif