/*
**********************************
*      BotCore Custom Boss       *
*   Ruarzog, Naxril, and Wixar   *
*        Type: Boss Encounter    *
*         By Josh Carter         *
**********************************
*/

/* ScriptData
SDCategory: Karazhan
SDName: Boss_Ruarzog
SD%Complete: 100%
SDComment: Needs in game test
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_wizards.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum Yells
{
	SAY_AGGRO									= 0, // 0 == Group ID for Creature_Text Table -> Sound included.
	SAY_PATHETIC								= 1,
	SAY_SLAY									= 2,
	SAY_RUNAWAY									= 3, // use @ run away
};

class boss_ruarzog : public CreatureScript
{
public:
	boss_ruarzog() : CreatureScript("boss_ruarzog") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_ruarzogAI (creature);
	}

	struct boss_ruarzogAI : public ScriptedAI
	{
		boss_ruarzogAI(Creature* creature) : ScriptedAI(creature) {}

		uint32 SoulreaperTimer;
		uint32 FlamestrikeTimer;
		uint32 LavaBombTimer;
		uint32 ReinofchaosTimer;
		uint32 EnrageTimer;
		uint32 DeathTimer;

		void Reset()
		{
			SoulreaperTimer       = urand(45000, 65000);
			FlamestrikeTimer      = urand(15000, 60000);
			LavaBombTimer         = urand(30000, 75000);
			ReinofchaosTimer	  = urand(15000, 95000);
			EnrageTimer			  = 600000;	// 10 min Enrage Timer
			DeathTimer			  = 900000; // Kill all after 15 min
		}

		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			Talk(SAY_PATHETIC);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			Talk(SAY_SLAY);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			Talk(SAY_AGGRO);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if (!UpdateVictim())
				return;

			if (DeathTimer <=diff && !DeathTimer)
			{
				DoCast(me, SPELL_FURY_OF_FROSTMOUNE, true);
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
				DeathTimer = urand(10000, 15000);
			}
			else
				DeathTimer -= diff;

			if (EnrageTimer <=diff && !EnrageTimer)
			{
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
			}
			else
				EnrageTimer -= diff;

			if (FlamestrikeTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE);
					DoCast(Target, SPELL_CHARRED_EARTH);
					Talk(SAY_RUNAWAY);
					FlamestrikeTimer = urand(25000, 85000);
				}
			} else FlamestrikeTimer -= diff;

			if (SoulreaperTimer <= diff)
			{
				DoCastVictim(SPELL_SOUL_REAPER, true);
				me->MonsterYell("I will split your soul into fragments and consume it!!!", LANG_UNIVERSAL, me);
				SoulreaperTimer = urand(45000, 65000);
			} else SoulreaperTimer -= diff;

			if (LavaBombTimer <= diff)
			{
				DoCastVictim(SPELL_LAVA_BOMB, true);
				me->MonsterYell("Feel my rage!!!", LANG_UNIVERSAL, me);
				LavaBombTimer = urand(30000, 75000);
			}
			else
				LavaBombTimer -= diff;

			if (ReinofchaosTimer <= diff)
			{
				me->MonsterYell("Let the fires of Heaven rain down upon you!!!", LANG_UNIVERSAL, me);
				DoCastVictim(SPELL_CHARRED_EARTH, true);
				DoCastVictim(SPELL_RAIN_OF_CHAOS, true);
				ReinofchaosTimer = urand(10000, 65000);
			}
			else
				ReinofchaosTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_ruarzog()
{
	new boss_ruarzog();
}