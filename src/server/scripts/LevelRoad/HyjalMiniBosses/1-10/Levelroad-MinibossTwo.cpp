/*
**********************************
*      Icarus-Gaming Inc.        *
*	MiniBossTwo Level Road IA    *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 20 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_ARCANE_BLAST				= 24857, // Blasts an enemy with Arcane magic, inflicting normal damage plus 1050 - 1350 and knocking the enemy back.
SPELL_ARCANE_EXPLOSION			= 8438, // Causes an explosion of arcane magic around the caster, causing 97 - 105 Arcane damage to all targets within 10 yards.
SPELL_ARCANE_MISSLES			= 5145, // Launches Arcane Missiles at the enemy, causing 56 Arcane damage every 5 sec for 5 sec.
SPELL_BOLT_OF_EONAR				= 30606, // Hurls a magical bolt at an enemy, inflicting 85 - 115 Nature damage.
SPELL_ARCANE_POTENCY			= 24544, // Increases arcane spell power by 200 for 20 sec
};

class npc_minibosstwo : public CreatureScript
{
public:
	npc_minibosstwo() : CreatureScript("npc_minibosstwo") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibosstwoAI (creature);
	}

	struct npc_minibosstwoAI : public ScriptedAI
	{
		npc_minibosstwoAI(Creature* creature) : ScriptedAI(creature) {}
	
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 ArcaneBlastTimer;
			uint32 ArcaneExplosionTimer;
			uint32 ArcaneMissleTimer;
			uint32 BoltofEonarTimer;
			uint32 ArcanePotencyTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			ArcaneBlastTimer = urand (30000, 35000);
			ArcaneExplosionTimer = urand(45000, 60000);
			ArcaneMissleTimer = urand(60000, 65000);
			BoltofEonarTimer = urand(60000, 90000);
			ArcanePotencyTimer = 20000; // timer 20 seconds, lasts 20 seconds
			MiniBossInCombat = false;\
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 39200); // Greiving Spell-Blade
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("HAHAHA!!! You're Dead You Little Slug... ", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("Oh... It... Hurts...", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Ahhh... Look, my meal came right to me!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("I grow bored just standing around here all day... I need a group of Heroes to eat!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000); // Changed to 1 Min
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("... the tedium... it irks me...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); // Changed to 1 Min
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("Oh, my, God... Will the boredom never end?", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = urand(25000, 80000); // Changed to 1 Min
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (ArcanePotencyTimer <= diff)
					{
					DoCast(me, SPELL_ARCANE_POTENCY, true);
					ArcanePotencyTimer = 20000;
					}
					else ArcanePotencyTimer -= diff;
				
				if (ArcaneBlastTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_ARCANE_BLAST);
						me->MonsterYell("Blast you little 'Hero'!!!", LANG_UNIVERSAL, me);
						ArcaneBlastTimer = urand(30000, 45000);
						}
					}else ArcaneBlastTimer -= diff;
				
				if (ArcaneExplosionTimer <= diff)
					{
					DoCast(me, SPELL_ARCANE_EXPLOSION, true);
					DoCast(me, SPELL_ARCANE_EXPLOSION, true);
					DoCast(me, SPELL_ARCANE_EXPLOSION, true);
					me->MonsterYell("Too much Energy,... I'll just lets some go!!!", LANG_UNIVERSAL, me);
					ArcaneExplosionTimer = urand(50000, 65000);
					}else ArcaneExplosionTimer -= diff;
					
				if (ArcaneMissleTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_ARCANE_MISSLES);
						me->MonsterYell("Hmmm... who should I kill... Oh, You!!!", LANG_UNIVERSAL, me);
						ArcaneMissleTimer = urand(30000, 45000);
						}
					}else ArcaneMissleTimer -= diff;
									
					if (BoltofEonarTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_BOLT_OF_EONAR);
						me->MonsterYell("Feel my power little 'Hero'!!!", LANG_UNIVERSAL, me);
						BoltofEonarTimer = urand(50000, 70000);
						}
					}else BoltofEonarTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibosstwo()
{
	new npc_minibosstwo();
}		