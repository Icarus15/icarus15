/*
**********************************
*      Icarus-Gaming Inc.        *
*  MiniBossThree Level Road IA   *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 30 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_CRUSH						= 59330, // Deals 200% melee damage to your target, knocking them down for 2 sec.
SPELL_ARC_SMASH					= 40457, // Lashes out in a vicious arc, inflicting normal damage plus 125 to enemies in a cone in front of the caster.
SPELL_HEAD_SMASH				= 52316, // Lashes out in a vicious head slam, inflicting 145% of normal damage to enemies in a cone in front of the caster.
SPELL_1H_SMASH					= 63573, // The not so massive attack Inflicts Physical damage and decreases armor by 20% for 6 sec.
SPELL_TUSKAR_SMASH				= 55763, // Smashes all enemies in a 10 yard radius for 4375 - 5625 damage!
};

class npc_minibossthree : public CreatureScript
{
public:
	npc_minibossthree() : CreatureScript("npc_minibossthree") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibossthreeAI (creature);
	}

	struct npc_minibossthreeAI : public ScriptedAI
	{
		npc_minibossthreeAI(Creature* creature) : ScriptedAI(creature) {}
	
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 CrushTimer;
			uint32 ArcSmashTimer;
			uint32 HeadSmashTimer;
			uint32 OneHandSmashTimer;
			//uint32 TuskarSmashTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			CrushTimer = urand (30000, 55000);
			ArcSmashTimer = urand(45000, 60000);
			HeadSmashTimer = 20000;
			OneHandSmashTimer = 30000;
			//TuskarSmashTimer = 60000; 
			MiniBossInCombat = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 37169); // War Mace of Unrequited Love
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Opps.  I think I squished this one...", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("Pain... So Much Pain!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("So many squishies all together!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("Stomp, stomp, stomp... stomp and squish... squish, squish, squish!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000);
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("Oooh.... look a squirrel!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); 
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("La Da De Dum... I'm bored!!! I need something to stomp on!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = urand(25000, 80000); 
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (CrushTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_CRUSH);
						me->MonsterYell("Smashy, smash you good!!!", LANG_UNIVERSAL, me);
						CrushTimer = urand(30000, 45000);
						}
					}else CrushTimer -= diff;
				
				if (ArcSmashTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_ARC_SMASH);
					me->MonsterYell("You not so tough, I smash good!!!", LANG_UNIVERSAL, me);
					ArcSmashTimer = urand(50000, 65000);
					}else ArcSmashTimer -= diff;
					
				if (HeadSmashTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_HEAD_SMASH);
					me->MonsterYell("I'll bonk you with me head!!!", LANG_UNIVERSAL, me);
					HeadSmashTimer = urand(45000, 75000);
					}else HeadSmashTimer -= diff;
									
					if (OneHandSmashTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_1H_SMASH, true);
						me->MonsterYell("Feel my power little 'Hero'!!!", LANG_UNIVERSAL, me);
						OneHandSmashTimer = urand(50000, 70000);
						}
					}else OneHandSmashTimer -= diff;
					
					/*if (TuskarSmashTimer <= diff)
					{
					DoCast(me, SPELL_TUSKAR_SMASH, true);
					me->MonsterYell("CRUSH YOU ALL!!!", LANG_UNIVERSAL, me->GetGUID());
					TuskarSmashTimer = urand(45000, 75000);
					}else TuskarSmashTimer -= diff;*/
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibossthree()
{
	new npc_minibossthree();
}		