/*
* NPC Server Helper
* By Josh Carter
* Only for Icarus-Gaming
* © 2013 Icarus Gaming Inc.
*/

/*
* GOSSIP_ICON_CHAT                = 0      White chat bubble
* GOSSIP_ICON_VENDOR              = 1      Brown bag
* GOSSIP_ICON_TAXI                = 2      Flight
* GOSSIP_ICON_TRAINER             = 3      Book
* GOSSIP_ICON_INTERACT_1          = 4      Interaction wheel
* GOSSIP_ICON_INTERACT_2          = 5      Interaction wheel
* GOSSIP_ICON_MONEY_BAG           = 6      Brown bag with yellow dot
* GOSSIP_ICON_TALK                = 7      White chat bubble with black dots
* GOSSIP_ICON_TABARD              = 8      Tabard
* GOSSIP_ICON_BATTLE              = 9      Two swords
* GOSSIP_ICON_DOT                 = 10     Yellow dot
* GOSSIP_ICON_CHAT_11             = 11,    White chat bubble
* GOSSIP_ICON_CHAT_12             = 12,    White chat bubble
* GOSSIP_ICON_CHAT_13             = 13,    White chat bubble
* GOSSIP_ICON_UNK_14              = 14,    INVALID - DO NOT USE
* GOSSIP_ICON_UNK_15              = 15,    INVALID - DO NOT USE
* GOSSIP_ICON_CHAT_16             = 16,    White chat bubble
* GOSSIP_ICON_CHAT_17             = 17,    White chat bubble
* GOSSIP_ICON_CHAT_18             = 18,    White chat bubble
* GOSSIP_ICON_CHAT_19             = 19,    White chat bubble
* GOSSIP_ICON_CHAT_20             = 20,    White chat bubble
*/

#include "ScriptPCH.h"
#include "Config.h"

#define EMOTE_FUNCTION_OFF "Function Disable!"

enum Actions
{
	// Level Roads
	ACTION_GO_HYJAL		                 = 10, 
	ACTION_GO_PANDARIA					 = 11,
	ACTION_GO_VASHJIR					 = 12,
	ACTION_NOT_YET						 = 13,
	//INSTANT 255
	ACTION_LEVEL_MAX					 = 14,
	// STARTER GEAR
	ACTION_STARTER_WARRIOR				 = 15,
	ACTION_STARTER_PALLY				 = 16,	
	ACTION_STARTER_PRIEST                = 17,
	ACTION_STARTER_ROGUE				 = 18,
	ACTION_STARTER_HUNTER   			 = 19,
	ACTION_STARTER_SHAMMY				 = 20,	
	ACTION_STARTER_DRU                   = 21,
	ACTION_STARTER_MAGE					 = 22,
	ACTION_STARTER_WARLOCK				 = 23,
	ACTION_STARTER_DK					 = 24,
	// 255 GEAR
	ACTION_MAXG_WARRIOR					 = 25,
	ACTION_MAXG_PALLY					 = 26,
	ACTION_MAXG_PRIEST 					 = 27,
	ACTION_MAXG_ROGUE					 = 28,
	ACTION_MAXG_HUNTER					 = 29,
	ACTION_MAXG_SHAMMY					 = 30,
	ACTION_MAXG_DRU						 = 31,
	ACTION_MAXG_MAGE					 = 32,
	ACTION_MAXG_WARLOCK					 = 33,
	ACTION_MAXG_DK						 = 34,
	// 255 Gear Sub Menus 
	ACTION_MAXG_WARRIOR_TANK			 = 35,
	ACTION_MAXG_WARRIOR_DPS				 = 36,
	ACTION_MAXG_PALLY_TANK				 = 37,
	ACTION_MAXG_PALLY_DPS				 = 38,
	ACTION_MAXG_PALLY_HEALER			 = 39,
	
};

enum StarterItemIds
{
ITEM_INTRAVENOUS_HEALING_POTION = 44698, //Test Item
// Starter Weapons
ITEM_MELEE_STARTER_SWORD = 37401,
ITEM_MELEE_STARTER_DAGGER = 47676,
ITEM_RANGE_STARTING_BOW = 39829,
ITEM_ALL_STARTING_STAFF = 40455,
ITEM_MAGIC_STARTING_WAND = 45511, 
// Warrior Starter Armor
ITEM_WARRIOR_STARTER_BRACERS = 16861,
ITEM_WARRIOR_STARTER_SABATONS = 16862,
ITEM_WARRIOR_STARTER_GUANTLETS = 16863,
ITEM_WARRIOR_STARTER_BELT = 16864,	
ITEM_WARRIOR_STARTER_BREASTPLATE = 16865,
ITEM_WARRIOR_STARTER_HELM = 16866,
ITEM_WARRIOR_STARTER_LEGPLATES = 16867,
ITEM_WARRIOR_STARTER_PAULDRONS = 16868,
// Paladin Starter Armor
ITEM_PALADIN_STARTER_BRACERS = 16857,
ITEM_PALADIN_STARTER_GUANTLETS = 16860,
ITEM_PALADIN_STARTER_CHESTGUARD = 16853,
ITEM_PALADIN_STARTER_HELM = 16854,
ITEM_PALADIN_STARTER_LEGPLATES = 16855,
ITEM_PALADIN_STARTER_PAULDRONS = 16856,
ITEM_PALADIN_STARTER_BOOTS = 16859,
// Priest Starter Armor
ITEM_PRIEST_STARTER_VAMBRACES = 16819,
ITEM_PRIEST_STARTER_BOOTS = 16811,
ITEM_PRIEST_STARTER_GLOVES = 16812,
ITEM_PRIEST_STARTER_GIRDLE = 16817,	
ITEM_PRIEST_STARTER_ROBES = 16815,
ITEM_PRIEST_STARTER_CIRCLET = 16813,
ITEM_PRIEST_STARTER_PANTS = 16814,
ITEM_PRIEST_STARTER_MANTLE = 16816,
// Hunter Starter Armor					
ITEM_HUNTER_STARTER_BRACERS = 16850,
ITEM_HUNTER_STARTER_BOOTS = 16849,
ITEM_HUNTER_STARTER_GLOVES = 16852,
ITEM_HUNTER_STARTER_BELT = 16851,	
ITEM_HUNTER_STARTER_BREASTPLATE = 16845,
ITEM_HUNTER_STARTER_HELMET = 16846,
ITEM_HUNTER_STARTER_LEGGINGS = 16847,
ITEM_HUNTER_STARTER_EPAULETS = 16848,
// Rogue Starter Armor
ITEM_ROGUE_STARTER_BRACERS = 16825,
ITEM_ROGUE_STARTER_BOOTS = 16824,
ITEM_ROGUE_STARTER_GLOVES = 16826,
ITEM_ROGUE_STARTER_BELT = 16827,	
ITEM_ROGUE_STARTER_CHESTPIECE = 16820,
ITEM_ROGUE_STARTER_COVER = 16821,
ITEM_ROGUE_STARTER_PANTS = 16822,
ITEM_ROGUE_STARTER_SHOULDERPADS = 16823,
// Shaman Starter Armor
ITEM_SHAMMY_STARTER_BRACERS = 16840,
ITEM_SHAMMY_STARTER_BOOTS = 16837,
ITEM_SHAMMY_STARTER_GAUNTLETS = 16839,
ITEM_SHAMMY_STARTER_BELT = 16838,	
ITEM_SHAMMY_STARTER_VESTMENTS = 16841,
ITEM_SHAMMY_STARTER_HELMET = 16842,
ITEM_SHAMMY_STARTER_LEGGINGS = 16843,
ITEM_SHAMMY_STARTER_EPAULETS = 16844,
// Mage Starter Armor
ITEM_MAGE_STARTER_BINDINGS = 16799,
ITEM_MAGE_STARTER_BOOTS = 16800,
ITEM_MAGE_STARTER_GLOVES = 16801,
ITEM_MAGE_STARTER_BELT = 16802,	
ITEM_MAGE_STARTER_ROBES = 16798,
ITEM_MAGE_STARTER_CROWN = 16795,
ITEM_MAGE_STARTER_LEGGINGS = 16796,
ITEM_MAGE_STARTER_MANTLE = 16797,
// Warlock Starter Armor
ITEM_WARLOCK_STARTER_BRACERS = 16804,
ITEM_WARLOCK_STARTER_SLIPPERS = 16803,
ITEM_WARLOCK_STARTER_GLOVES = 16805,
ITEM_WARLOCK_STARTER_BELT = 16806,	
ITEM_WARLOCK_STARTER_ROBES = 16809,
ITEM_WARLOCK_STARTER_HORNS = 16808,
ITEM_WARLOCK_STARTER_PANTS = 16810,
ITEM_WARLOCK_STARTER_SHOULDER = 16807,
// Druid Starter Armor
ITEM_DRUID_STARTER_BRACERS = 16830,
ITEM_DRUID_STARTER_BOOTS = 16829,
ITEM_DRUID_STARTER_GLOVES = 16831,
ITEM_DRUID_STARTER_BELT = 16828,	
ITEM_DRUID_STARTER_VESTMENTS = 16833,
ITEM_DRUID_STARTER_HELM = 16834,
ITEM_DRUID_STARTER_LEGGINGS = 16835,
ITEM_DRUID_STARTER_SPAULDERS = 16836,
// Death Knight Starter Armor
ITEM_DK_STARTER_WRISTGUARD = 34653,
ITEM_DK_STARTER_GREAVES = 34648,
ITEM_DK_STARTER_GAUNTLETS = 34649,
ITEM_DK_STARTER_GIRDLE = 34651,	
ITEM_DK_STARTER_TUNIC = 34650,
ITEM_DK_STARTER_HOOD = 34652,
ITEM_DK_STARTER_LEGGPLATES = 34656,
ITEM_DK_STARTER_PAULDRONS = 34655,
};

//enum MaxGItemIds
//{ }
