#ifndef DEF_FIRELANDS_H
#define DEF_FIRELANDS_H

enum DataTypes
{
	TYPE_RAGEMAUL					= 1,
};

#define ERROR_INST_DATA(a)          TC_LOG_ERROR(LOG_FILTER_TSCR, "Instance Data for Firelands not set properly. Encounter for Creature Entry %u may not work properly.", a->GetEntry());
#endif